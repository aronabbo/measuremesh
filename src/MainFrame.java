import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.concurrent.ExecutionException;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JTextPane;
import javax.swing.SwingWorker;
import javax.swing.filechooser.FileNameExtensionFilter;

import math.Mesh3d;
import modelLoader.Model3dLoader;
import modelLoader.Model3dLoaderFactory;
import modelLoader.ParseException;


public class MainFrame extends JFrame{
	public MainFrame() {
		// initialize GUI elements
		setTitle("MeasureMesh");
		
		JTextPane messagePane = new JTextPane();
		messagePane.setEditable(false);
		messagePane.setText("Welcome to MeasureMesh! \n\nTo see the surface and volume of a 3d file, select a file below!");
		messagePane.setPreferredSize(new Dimension(400, 200));
		messagePane.setBorder(BorderFactory.createEmptyBorder(40, 20, 20, 20));
		
		JButton openFileButton = new JButton("Pick a 3d file...");
		openFileButton.setPreferredSize(new Dimension(50, 30));
		
		JFileChooser fileChooser = new JFileChooser();
		fileChooser.setFileFilter(new FileNameExtensionFilter("obj, stl", "obj", "stl"));
		
		// add logic
		openFileButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				
				if(fileChooser.showOpenDialog(MainFrame.this) == JFileChooser.APPROVE_OPTION )
				{
					messagePane.setText("analyzing file: " + fileChooser.getSelectedFile());
					
					new SwingWorker<Void, Void>() {

						@Override
						protected Void doInBackground() {
							try {
								File file = fileChooser.getSelectedFile();
								Model3dLoader loader = Model3dLoaderFactory.createModelLoader(file);
								Mesh3d mesh = loader.load(file);
								
								DecimalFormat f = new DecimalFormat("##.00");
								
								String message = "Analyzed file: " + file.getName();
								
								message += "\n\npolygons:\t" + mesh.getTriangles().size();
								message += "\nsurface area:\t" + f.format(mesh.getSurface());
								message += "\nvolume:      \t" + f.format(mesh.getVolume());
								
								messagePane.setText(message);
								
								openFileButton.setText("Pick another 3d file...");
							} catch (IOException | ParseException e) {
								messagePane.setText("Error while parsing 3d model file: " + e.getMessage());
								
								System.out.print("\nerror while parsing 3d model file: ");
								e.printStackTrace();
							}
							return null;
						}
						
						protected void done() {
							try {
								get();
							} catch (InterruptedException | ExecutionException e) {
								messagePane.setText("Error while parsing 3d model file: " + e.getMessage());
								
								System.out.print("\nerror while parsing 3d model file: ");
								e.printStackTrace();
							}
						};
						
					}.execute();
				}
			}
		});
		
		// build the layout
		getContentPane().add(messagePane, BorderLayout.NORTH);
		getContentPane().add(openFileButton, BorderLayout.CENTER);
		
		pack();
	}
	
	private static final long serialVersionUID = 1L;
}
