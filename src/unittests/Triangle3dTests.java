package unittests;

import static org.junit.Assert.assertEquals;

import javax.vecmath.Vector3d;

import math.Triangle3d;

import org.junit.Test;

public class Triangle3dTests {
	@Test
	public void test2dSurface() {
		Triangle3d t = new Triangle3d(
				new Vector3d(100, 0, 0),
				new Vector3d(0, 100, 0),
				new Vector3d(100, 100, 0)
				);
		
		assertEquals(5000d, t.getSurface(), 0.001);
	}

	@Test
	public void test3dSurface() {
		Triangle3d t = new Triangle3d(
				new Vector3d(0, 0, 0),
				new Vector3d(1, 0, 1),
				new Vector3d(0, 1, 1)
				);
		
		assertEquals(0.866025d, t.getSurface(), 0.001);
	}	
}
