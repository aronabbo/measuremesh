package unittests;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import math.Mesh3d;
import modelLoader.ObjectFileLoader;
import modelLoader.ParseException;
import modelLoader.StlFileLoader;

import org.junit.Test;

/**
 * Run general tests that load 3d files and calculate the volumes and surfaces
 * of the files.
 * 
 * @author Aron
 */
public class SurfaceAndVolumeTests {
	
	private static final String TEST_DATA_FOLDER = "testData";
	
	/**
	 * Allowed error for volume and surface calculations of STL files.
	 */
	private static final double STL_ERROR_DELTA = 0.01;
	
	/**
	 * Allowed error for volume and surface calculations of OBJ files. Values
	 * are saved with less precision in OBJ files, so error is greater compared
	 * to the source files that they are exported from.
	 */
	private static final double OBJ_ERROR_DELTA = 5;
	
	@Test
	public void testBinaryStlVolume() throws IOException, ParseException {
		StlFileLoader loader = new StlFileLoader();
		
		System.out.println("\ntesting binary stl volume");
		
		for(TestInfo testData : _modelBaseNames)
		{
			System.out.println("\ntesting file: " + testData.getName());
			Mesh3d mesh = loader.load( new File(TEST_DATA_FOLDER + "/" +  testData.getName() + "Binary.stl") );
			
			assertEquals(testData.getVolume(), mesh.getVolume(), STL_ERROR_DELTA);
		}
	}
	
	@Test
	public void testBinaryStlSurface() throws IOException, ParseException {
		StlFileLoader loader = new StlFileLoader();
		
		System.out.println("\ntesting binary stl surface");
		
		for(TestInfo testData : _modelBaseNames)
		{
			System.out.println("\ntesting file: " + testData.getName());
			Mesh3d mesh = loader.load( new File(TEST_DATA_FOLDER + "/" +  testData.getName() + "Binary.stl") );
			
			assertEquals(testData.getSurface(), mesh.getSurface(), STL_ERROR_DELTA);
		}
	}
	
	@Test
	public void testAsciiStlVolume() throws IOException, ParseException {
		StlFileLoader loader = new StlFileLoader();
		
		System.out.println("\ntesting ascii stl volume");
		
		for(TestInfo testData : _modelBaseNames)
		{
			System.out.println("\ntesting file: " + testData.getName());
			Mesh3d mesh = loader.load( new File(TEST_DATA_FOLDER + "/" +  testData.getName() + "Ascii.stl") );
			
			assertEquals(testData.getVolume(), mesh.getVolume(), STL_ERROR_DELTA);
		}
	}
	
	@Test
	public void testAsciiStlSurface() throws IOException, ParseException {
		StlFileLoader loader = new StlFileLoader();
		
		System.out.println("\ntesting ascii stl surface");
		
		for(TestInfo testData : _modelBaseNames)
		{
			System.out.println("\ntesting file: " + testData.getName());
			Mesh3d mesh = loader.load( new File(TEST_DATA_FOLDER + "/" +  testData.getName() + "Ascii.stl") );
			
			assertEquals(testData.getSurface(), mesh.getSurface(), STL_ERROR_DELTA);
		}
	}
	
	@Test
	public void testObjFileVolume() throws IOException, ParseException {
		ObjectFileLoader loader = new ObjectFileLoader();
		
		System.out.println("\ntesting obj volume");
		
		for(TestInfo testData : _modelBaseNames)
		{
			System.out.println("\ntesting file: " + testData.getName());
			Mesh3d mesh = loader.load( new File(TEST_DATA_FOLDER + "/" +  testData.getName() + ".obj") );
			
			// obj files store coordinates in lower precision, so 
			assertEquals(testData.getVolume(), mesh.getVolume(), OBJ_ERROR_DELTA);
		}
	}
	
	@Test
	public void testObjFileSurface() throws IOException, ParseException {
		ObjectFileLoader loader = new ObjectFileLoader();
		
		System.out.println("\ntesting obj surface");
		
		for(TestInfo testData : _modelBaseNames)
		{
			System.out.println("\ntesting file: " + testData.getName());
			Mesh3d mesh = loader.load( new File(TEST_DATA_FOLDER + "/" +  testData.getName() + ".obj") );
			
			assertEquals(testData.getSurface(), mesh.getSurface(), OBJ_ERROR_DELTA);
		}
	}
	
	/**
	 * Volume and surface values are determined using 
	 * Sketchup and the Volume Calculator plugin.
	 */
	private List<TestInfo> _modelBaseNames = Arrays.asList(
			new TestInfo("Tetrahedron", 	166666.666667, 	23660.254038),
			new TestInfo("Cube", 			1000000, 		60000),
			new TestInfo("NegativeCube", 	1000000, 		60000),
			new TestInfo("RotatedCube", 	1000000, 		60000),
			new TestInfo("ConcaveCube", 	21000, 			6200),
			new TestInfo("Teapot", 			3195.342097, 	1273.643501)
			);
	
	private class TestInfo{
		private String _name;
		private double _volume;
		private double _surface;

		public TestInfo(String name, double volume, double surface) {
			_name = name;
			_volume = volume;
			_surface = surface;
		}
		
		private String getName() {
			return _name;
		}
		
		private double getVolume() {
			return _volume;
		}
		
		private double getSurface() {
			return _surface;
		}
	}
}
