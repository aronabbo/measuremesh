import java.io.File;
import java.io.IOException;

import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import math.Mesh3d;
import modelLoader.Model3dLoader;
import modelLoader.Model3dLoaderFactory;
import modelLoader.ParseException;

/**
 * Main entrypoint: suitable both for command-line and GUI mode.
 * 
 * For GUI mode, launch the application with no arguments.
 * 
 * This opens a small GUI that enables a user to load arbitrary .obj and .stl
 * files. After loading a 3d file, the volume and surface are displayed.
 * 
 * For command-line mode, launch the application with one parameter: the
 * location of the 3d model that should be parsed. Volume and surface will be
 * calculated and displayed. If more than one argument is given, a help message
 * is shown.
 * 
 * @author Aron
 */
public class Launcher {
	public static void main(String[] args) throws ClassNotFoundException, InstantiationException, IllegalAccessException, UnsupportedLookAndFeelException {
		if(args.length == 0)
		{
			// go into gui mode
			
			// make the GUI look less java-ish
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
			MainFrame frame = new MainFrame();
			frame.setVisible(true);
		}
		else if (args.length != 1) {
			// incorrect use: show help message
			System.out.println("\n usage: measureMesh [path/to/file.ext]");
			System.out.println("\n the mesh of the 3d model in the file is assumed to be closed and not intersecting itself.");
			System.exit(0);
		}
		else
		{
			// command line mode
			String fileName = args[0]; 
			
			System.out.println("\n analyzing file: " + fileName);

			try {
				File file = new File(fileName);
				Model3dLoader loader = Model3dLoaderFactory.createModelLoader(file);
				Mesh3d mesh = loader.load(file);
				
				System.out.print("\npolygons:    \t" + mesh.getTriangles().size());
				System.out.print("\nsurface area:\t" + mesh.getSurface());
				System.out.print("\nvolume:      \t" + mesh.getVolume());

			} catch (IOException | ParseException e) {
				System.out.print("\nerror while parsing 3d model file: ");
				e.printStackTrace();
			}

			System.exit(0);	
		}
	}
}
