package math;

import java.util.ArrayList;
import java.util.List;

/**
 * A 3d mesh consisting of a collection of triangular polygons. Does not support
 * polygons with more than 3 vertices.
 * 
 * @author Aron
 */
public class Mesh3d {
	
	/**
	 * Create an empty mesh.
	 */
	public Mesh3d() {
		_triangles = new ArrayList<Triangle3d>();
	}
	
	/**
	 * @param triangle a triangle to be added to this mesh.
	 */
	public void add(Triangle3d triangle)
	{
		_triangles.add(triangle);
	}
	
	/**
	 * @param triangles
	 *            a list of triangles to be added to this mesh.
	 */
	public void add(List<Triangle3d> triangles) {
		_triangles.addAll(triangles);
	}

	/**
	 * @return the list of triangles that this mesh consist of
	 */
	public List<Triangle3d> getTriangles() {
		return _triangles;
	}

	/**
	 * @return the volume of this mesh. Assumes that the triangles of this mesh
	 *         form one or more closed shapes that do not intersect themselves
	 *         or each other. If these conditions are not met, the returned
	 *         value is not meaningful.
	 * 
	 *         Volume is calculated using the addition of the signed volumes of
	 *         each trianglular polygon of the mesh, as described by Cha Zhang
	 *         and Tsuhan Chen (
	 *         http://research.microsoft.com/en-us/um/people/chazhang
	 *         /publications/icip01_ChaZhang.pdf ).
	 * 
	 *         The computational complexity is O(N), with N the amount of
	 *         triangles in the mesh.
	 */
	public double getVolume( ) {
		double volume = 0;
		
		for(Triangle3d triangle : _triangles)
		{
			volume += triangle.getSignedVolumeFromOrigin();
		}
		
	    return Math.abs(volume);
	}
	
	/**
	 * @return the combined surface of the mesh. It only counts the outside of
	 *         the polygons of the mesh, so for an open mesh, all polygons are
	 *         counted once. Overlapping polygons are all counted.
	 * 
	 *         The computational complexity is O(N), with N the amount of
	 *         triangles in the mesh.
	 */
	public double getSurface() {
		double surface = 0;
		
		for(Triangle3d triangle : _triangles)
		{
			surface += triangle.getSurface();
		}
		
	    return surface;
	}
	
	private List<Triangle3d> _triangles;
}
