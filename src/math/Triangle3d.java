package math;

import java.util.ArrayList;
import java.util.Arrays;

import javax.vecmath.Vector3d;

/**
 * A triangle in 3d space, which spans a surface. The direction of the normal is
 * outward. The vertices are listed in counterclockwise order when looking at
 * the object from the outside (right-hand rule).
 * 
 * @author Aron
 */
public class Triangle3d {
	/**
	 * Create a new triangle from three vertices
	 */
	public Triangle3d(Vector3d point1, Vector3d point2, Vector3d point3) {
		_points = new ArrayList<Vector3d>(Arrays.asList(point1, point2, point3));
	}

	@Override
	public String toString() {
		return super.toString() + "\t[" + _points.get(0) + ", "
				+ _points.get(1) + ", " + _points.get(2) + "]";
	}

	/**
	 * @param index
	 *            index of a triangle, ranging 0...2. Throws runtime exception
	 *            if out of range.
	 * @return the point that corresponds to the index.
	 */
	public Vector3d getPoint(int index) {
		return _points.get(index);
	}

	/**
	 * @return the signed volume of the tetrahedron that is formed by this
	 *         triangle and the origin (0,0,0). The returned value will be positive if
	 *         the normal of this triangle is pointing away from the origin, and
	 *         it will be negative if it is pointing towards the origin.
	 */
	public double getSignedVolumeFromOrigin() {
		Vector3d crossProduct = new Vector3d();
		crossProduct.cross(_points.get(1), _points.get(2));
		
	    return _points.get(0).dot( crossProduct )/6d;
	}
	
	/**
	 * @return the surface of this triangle.
	 */
	public double getSurface() {
		Vector3d v1 = (Vector3d) _points.get(1).clone();
		v1.sub(_points.get(0));
		
		Vector3d v2 = (Vector3d) _points.get(2).clone();
		v2.sub(_points.get(0));
		
		Vector3d crossProduct = new Vector3d();
		crossProduct.cross(v1, v2);
		
		return crossProduct.length() * 0.5d;
	}
	
	private ArrayList<Vector3d> _points = new ArrayList<Vector3d>();
}
