package modelLoader;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PushbackInputStream;
import java.nio.ByteBuffer;
import java.util.ArrayList;

import javax.vecmath.Vector3d;

import math.Mesh3d;
import math.Triangle3d;

/**
 * Load and parse a binary or ASCII STL file. The loader will recognize whether
 * the file is ASCII or binary and parse accordingly.
 */
public class StlFileLoader implements Model3dLoader {
	@Override
	public Mesh3d load(File modelFile) throws IOException, ParseException {
		PushbackInputStream inputStream = null;
		try {
			inputStream = new PushbackInputStream(new FileInputStream(modelFile), 40);
			
			byte[] buffer = new byte[5];
			
			inputStream.read(buffer);
			
			String first5Chars = new String(buffer, "UTF-8");
			
			// push back the bytes so the parser can start from the beginning
			inputStream.unread(buffer);
			
			if( first5Chars.equals("solid") )
			{ 
				return parseAscii(inputStream);
			}
			else
			{
				return parseBinary(inputStream);
			}
			
		} catch (Exception e) {
			Model3dLoader.closeFile(inputStream);
			throw e;
		} finally {
			Model3dLoader.closeFile(inputStream);
		}
	}
	
	/**
	 * Load and parse an ASCII .stl file.
	 * 
	 * See http://www.fabbers.com/tech/STL_Format
	 * 
	 * @throws IOException
	 *             if there is an error reading the file stream
	 * @throws ParseException
	 *             if there is an error parsing the file stream
	 */
	private Mesh3d parseBinary(InputStream inputStream) throws IOException, ParseException {
		// skip the header
		inputStream.skip(80);

		// read the file in 4-byte chunks
		byte[] buffer = new byte[4];

		// parse the number of polygons in the file
		inputStream.read(buffer);
		int numPolygons = getIntWithLittleEndian(buffer);

		if (numPolygons > 100000) {
			throw new ParseException("Amount of reported polygons exceeds 100000, the file is likely corrupt.");
		}

		Mesh3d mesh = new Mesh3d();

		for (int polygonIndex = 0; polygonIndex < numPolygons; polygonIndex++) {
			ArrayList<Vector3d> currentTriangle = new ArrayList<Vector3d>();

			/*
			 * Discard the normal vectors. The front face of the triangle is
			 * also defined by the winding order of the vertices, and takes
			 * precedence.
			 */
			inputStream.skip(12);

			/*
			 * STL consists entirely out of triangular polygons with exactly 3
			 * vertices.
			 */
			for (int verticeIndex = 0; verticeIndex < 3; verticeIndex++) {
				inputStream.read(buffer);
				// System.out.println("buffer: " + buffer);

				float x = Float.intBitsToFloat(getIntWithLittleEndian(buffer));
				inputStream.read(buffer);
				float y = Float.intBitsToFloat(getIntWithLittleEndian(buffer));
				inputStream.read(buffer);
				float z = Float.intBitsToFloat(getIntWithLittleEndian(buffer));

				currentTriangle.add(new Vector3d(x, y, z));
			}

			if (currentTriangle.size() != 3) {
				throw new ParseException("Read error: a facet should contain 3 vertices");
			}

			mesh.add(new Triangle3d(
					currentTriangle.get(0), 
					currentTriangle.get(1), 
					currentTriangle.get(2)
					));

			// Skip Attribute bytes
			inputStream.skip(2);
		}
		return mesh;

	}

	/**
	 * Load and parse a binary .stl file.
	 * 
	 * See: http://www.fabbers.com/tech/STL_Format
	 * Assumes little-endian bytes (which is the STL standard)
	 * 
	 * @throws IOException
	 *             if there is an error reading the file stream
	 * @throws ParseException
	 *             if there is an error parsing the file stream
	 */
	private Mesh3d parseAscii(PushbackInputStream inputStream) throws IOException, ParseException {
		BufferedReader br = new BufferedReader ( new InputStreamReader(inputStream, "UTF-8") );
		
		String line;
		boolean readingFacet = false;
		
		ArrayList<Vector3d> currentTriangle = null;
		Mesh3d mesh = new Mesh3d();
		
		while((line = br.readLine()) != null) {
			if(line.startsWith("facet")) {
				if(readingFacet == true)
				{
					throw new ParseException("Read error: facet keyword encountered when last facet was not closed");
				}
				
				currentTriangle = new ArrayList<Vector3d>();
				
				readingFacet = true;
			}
			else if(readingFacet == true && line.contains("vertex")){
				Vector3d point = parseVertexLine(line);
				
				currentTriangle.add(point);
			}
			if(line.startsWith("endfacet")) {
				if(readingFacet == false) {
					throw new ParseException("Read error: endfacet keyword encountered when facet was not opened");
				}
				
				if(currentTriangle.size() < 3) {
					throw new ParseException("Read error: less than 3 vertices found in a facet");
				}
				
				mesh.add(new Triangle3d(
						currentTriangle.get(0),
						currentTriangle.get(1),
						currentTriangle.get(2)
						));
				currentTriangle = null;
				
				readingFacet = false;
			}
		}
		
		return mesh;
	}

	/**
	 * Parse a vertex line in the ASCII STL format.
	 * Should be of format:
	 *     vertex 100.0 100.0 100.0
	 * @throws ParseException if the format of the line is incorrect 
	 */
	private Vector3d parseVertexLine(String line) throws ParseException {
		String numbersString = line.substring( line.indexOf("vertex") + "vertex".length() );
		
		String[] numbers = numbersString.trim().split(" ");
		
		if(numbers.length != 3){
			throw new ParseException("Could not find three coordinates in: " + line);
		}
		
		return new Vector3d(
				Double.parseDouble(numbers[0]),
				Double.parseDouble(numbers[1]),
				Double.parseDouble(numbers[2])
				);
	}
	
	/**
	 * Read an integer from a buffer, in little-endian order.
	 */
	private static int getIntWithLittleEndian(byte[] buffer) {
		return ByteBuffer.wrap(buffer).order(java.nio.ByteOrder.LITTLE_ENDIAN).getInt();
	}
}
