package modelLoader;

/**
 * An exception that is thrown when a parsing error is encountered.
 * @author Aron
 */
public class ParseException extends Exception {
	public ParseException(String message, Throwable e) {
		super(message, e);
	}

	public ParseException(String message) {
		super(message);
	}

	private static final long serialVersionUID = 1L;
}
