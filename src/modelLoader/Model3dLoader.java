package modelLoader;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import math.Mesh3d;

/**
 * A loader that can load a 3d mesh from a file.
 * @author Aron
 */
public interface Model3dLoader {

	/**
	 * Load the mesh of a 3d file.
	 * 
	 * @param modelFile
	 *            a file which should contain a 3d model.
	 * @return a triangulized 3d mesh. The direction of the normals of the
	 *         triangles are outward. The vertices of each triangle are listed
	 *         in counterclockwise order when looking at the object from the
	 *         outside (right-hand rule).
	 * @throws FileNotFoundException
	 *             if the specified file does not exist.
	 * @throws IOException
	 *             if there is an error when opening the file, or if the Loader
	 *             is unable to parse the file.
	 * @throws ParseException 
	 */
	Mesh3d load(File modelFile) throws IOException, ParseException;
	
	/**
	 * Close an inputStream and show an error if any occurs
	 */
	public static void closeFile(InputStream inputStream) {
		try {
			if (inputStream != null)
				inputStream.close();
		} catch (IOException e) {
			System.out.println("error while closing 3d model file: ");
			e.printStackTrace();
		}
	}
}
