package modelLoader;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PushbackInputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.vecmath.Vector3d;

import math.Mesh3d;
import math.Triangle3d;

/**
 * A loader and parser for the Wavefront .obj file format. It currently does not fully support
 * files that contain polygons that have more than 3 vertices.
 * The precise limitations are described in {@link #triangulate(ArrayList)}.
 */
public class ObjectFileLoader implements Model3dLoader {
	@Override
	public Mesh3d load(File modelFile) throws IOException, ParseException {
		PushbackInputStream inputStream = null;
		try {
			inputStream = new PushbackInputStream(
					new FileInputStream(modelFile), 40);

			return parse(inputStream);

		} catch (Exception e) {
			Model3dLoader.closeFile(inputStream);
			throw e;
		} finally {
			Model3dLoader.closeFile(inputStream);
		}
	}

	/**
	 * Parse the inputStream of an .obj file.
	 * 
	 * @throws IOException
	 *             if there is a connection or parsing error while reading the
	 *             stream.
	 * @throws ParseException 
	 */
	private Mesh3d parse(PushbackInputStream inputStream)
			throws IOException, ParseException {

		BufferedReader br = new BufferedReader(new InputStreamReader(
				inputStream, "UTF-8"));

		String line;

		ArrayList<Vector3d> vertices = new ArrayList<Vector3d>();
		ArrayList<FaceIndexList> facesIndexMap = new ArrayList<FaceIndexList>();
		
		while ((line = br.readLine()) != null) {
			if (line.startsWith("v ")) {
				Vector3d parsedVertex = parseVertexLine(line);
				vertices.add(parsedVertex);
			} else if (line.startsWith("f ")) {
				facesIndexMap.add(parseFaceVertexIndexLine(line));
			}
		}

		try{
			return constructMesh(vertices, facesIndexMap);
		}
		catch(RuntimeException e)
		{
			throw new ParseException("Exception while constructing mesh from vertice and face data");
		}
	}

	/**
	 * Parse an .obj file line that contains vertex information.
	 * Should be of format: 
	 * v 0.123 0.234 0.345 1.0 
	 * or 
	 * v 0.123 0.234 0.345
	 * 
	 * @throws ParseException if the format is not supported
	 */
	private Vector3d parseVertexLine(String line) throws ParseException {
		String numbersString = line.substring(line.indexOf("v") + "v".length());
		
		String[] numbers = numbersString.trim().split(" ");
		
		if (numbers.length != 3 && numbers.length != 4) {
			throw new ParseException(
					"Could not find three coordinates in: " + line);
		}

		return new Vector3d(Double.parseDouble(numbers[0]),
				Double.parseDouble(numbers[1]), Double.parseDouble(numbers[2]));
	}

	/**
	 * Parse an .obj file vertex index line. A vertex index line 
	 * contains the indexes of several vertices (as they are parsed with 
	 * {@link #parseFaceVertexIndexLine(String)}).
	 * The line may have one of the following formats:
	 * 
	 * f 1 2 3 
	 * f 3/1 4/2 5/3 
	 * f 6/4/1 3/5/3 7/6/5 
	 * f 6//1 3//3 7//5
	 * 
	 * The vertex index is the first number in each space-separated block. 
	 * 1 corresponds to the first vertex encountered in the file, 2 to 
	 * the next vertex, etcetera.
	 * @throws ParseException if the format is not supported 
	 */
	private FaceIndexList parseFaceVertexIndexLine(String line) throws ParseException {
		try{
			FaceIndexList vertexIndexes = new FaceIndexList();
	
			String numbersString = line.substring(line.indexOf("f") + "f".length());
	
			/*
			 * vertexDescription has one of these formats: 1 3/1 6/4/1 6//1
			 */
			String[] vertexDescriptions = numbersString.trim().split(" ");
	
			for (String vertexDescriptionString : vertexDescriptions) {
				// take the first whole integer of the vertexDescription
				Integer vertexId = Integer.parseInt(vertexDescriptionString
						.split("/")[0]);
	
				vertexIndexes.add(vertexId);
			}
			return vertexIndexes;
		}
		catch(RuntimeException e)
		{
			throw new ParseException("Error parsing vertex", e);
		}
	}

	/**
	 * Create a mesh by combining a list of vertices and a list of polygon
	 * information that refer to the vertices.
	 * 
	 * @param vertices
	 *            a list of vertices, with their order corresponding to the
	 *            indexes of the vertices described by the facesIndexMap.
	 * @param facesIndexMap
	 *            List of {@link FaceIndexList}s. Every {@link FaceIndexList}
	 *            will be combined into a polygon using the vertices.
	 * @return a triangulated mesh. Polygons corresponding to more than 3
	 *         vertices will be triangulated with limitations described here:
	 *         {@link #triangulate(ArrayList)}.
	 * @throws ParseException 
	 */
	private Mesh3d constructMesh(ArrayList<Vector3d> vertices,
			ArrayList<FaceIndexList> facesIndexMap) throws ParseException {
		Mesh3d mesh = new Mesh3d();

		for (FaceIndexList singleFaceVertices : facesIndexMap) {
			ArrayList<Vector3d> faceVertices = new ArrayList<Vector3d>();
			
			for(Integer faceIndex : singleFaceVertices)
			{
				/*
				 * The vertex index as written in the .obj file starts at 1,
				 * while the vertice array starts at 0, so subtract 1.
				 */
				faceVertices.add( vertices.get(faceIndex - 1) );
			}
			
			if(faceVertices.size() == 4)
			{
				List<Triangle3d> faceVerticeTriangles = triangulate(faceVertices);
				mesh.add(faceVerticeTriangles);
			}
			else
			{
				Triangle3d triangle = new Triangle3d(
						faceVertices.get(0),
						faceVertices.get(1),
						faceVertices.get(2));
				mesh.add(triangle);
			}
		}

		return mesh;
	}

	/**
	 * Attempt to triangulate a polygon. Only convex polygons with 4 vertices
	 * are supported.
	 * 
	 * @param faceVertices
	 *            vertices that make up a polygon that should be converted into
	 *            triangles.
	 * @return a list of triangles that are equivalent to the polygon. If it contains a
	 *             polygon with 4 vertices that is concave, undesired results
	 *             can be generated.
	 * @throws ParseException
	 *             If a polygon has more than 4 vertices. 
	 */
	private List<Triangle3d> triangulate(ArrayList<Vector3d> faceVertices) throws ParseException {
		
		if (faceVertices.size() > 4) {
			/*
			 * It is possible to support polygons with more than 3 vertices
			 * by implementing a tesselation algorithm.
			 */
			throw new ParseException(
					"Support for polygons with more than 4 vertices is not implemented.");
		}
		
		/*
		 * Perform very simple triangulation of a polygon with 4 points by
		 * splitting it up in 2 triangles. Note that this will only work
		 * correctly when splitting up a concave polygon: Convex polygons can be
		 * split up incorrectly this way.
		 */
		return Arrays.asList(
				new Triangle3d(
						faceVertices.get(0),
						faceVertices.get(1),
						faceVertices.get(2)
						),
				new Triangle3d(
						faceVertices.get(2),
						faceVertices.get(3),
						faceVertices.get(0)
						)
				);
	}

	/**
	 * A list of vertex indexes that describe a polygon. The indexes correspond
	 * with the order in which the vertices are defined in the .obj file, with
	 * the first vertice being 1, the next 2, etcetera.
	 */
	private class FaceIndexList extends ArrayList<Integer> {
		private static final long serialVersionUID = 1L;

	}
}
