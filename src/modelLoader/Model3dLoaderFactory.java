package modelLoader;

import java.io.File;
import java.io.IOException;

/**
 * Factory class that determines the correct 3d model loader for a 3d file.
 * @author Aron
 */
public class Model3dLoaderFactory {
	/**
	 * Create the appropriate 3d file loader for a specific file.
	 * @throws IOException if no appropriate loader could be found.
	 */
	public static Model3dLoader createModelLoader(File modelFile) throws IOException {
		String lowerCaseFileName = modelFile.getName().toLowerCase();

		if (lowerCaseFileName.endsWith(".obj")) {
			ObjectFileLoader loader = new ObjectFileLoader();

			return loader;
		} else if (lowerCaseFileName.endsWith(".stl")) {
			StlFileLoader loader = new StlFileLoader();

			return loader;
		} else {
			throw new IOException(
					"The file format of the requested file is not supported: "
							+ modelFile.getName());
		}
	}
}
