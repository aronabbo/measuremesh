# MeasureMesh #

MeasureMesh is a small tool that calculates the volume and surface areas of 3d files.
Supported are .obj and .stl files.

## Requirements ##

MeasureMesh requires java 8+ to run.

### Command line ###
- download [measureMesh.jar](https://bitbucket.org/aronabbo/measuremesh/downloads/measureMesh.jar)
- run it with:
java -jar MeasureMesh.jar [path/to/file.ext]

### Gui mode ###
- download [measureMesh.jar](https://bitbucket.org/aronabbo/measuremesh/downloads/measureMesh.jar)
- run it with:
java -jar MeasureMesh.jar

or (on windows) right click in explorer -> open with... -> Java(TM) Platform SE Binary

![MesureMeshGui.png](https://bitbucket.org/repo/aM9AGz/images/1839851406-MesureMeshGui.png)

## Limitations ##

The volume calculation works well for trianglulated 3d meshes, but support for polygons with more than 3 vertices is limited.

To make sure that your volume is calculated correctly, make sure that:

- the 3d model is triangulized (this is always the case for .stl filesm but .obj files can contain polygons with more than 3 vertices, which is not supported)
- the 3d model consist of closed volumes (meaning: it does not have missing polygons that leave a hole in the model)
- the polygons of the 3d model do not intersect each other.

## Future improvements: ##

- add a triangulation algorithm to split up polygons with more than 3 vertices before doing calculations
- add consistency checks to detect holes and intersecting polygons